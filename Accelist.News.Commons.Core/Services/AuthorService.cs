﻿using Accelist.News.Commons.Core.Helpers;
using Accelist.News.Commons.Core.Models;
using Accelist.News.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.News.Commons.Core.Services
{
    public class AuthorService
    {
        private readonly NewsDbContext DB;

        /// <summary>
        /// Use the registered <seealso cref="NewsDbContext"/>.
        /// </summary>
        /// <param name="db">The registered <seealso cref="NewsDbContext"/> object.</param>
        public AuthorService(NewsDbContext db)
        {
            this.DB = db;
        }

        /// <summary>
        /// Get all authors.
        /// </summary>
        /// <returns>Return all authors in <seealso cref="AuthorViewModel"/> format.</returns>
        public async Task<List<AuthorViewModel>> GetAuthors()
        {
            var authors = await this
                .DB
                .Author
                .AsNoTracking()
                .Select(Q => new AuthorViewModel
                {
                    Name = Q.Name
                })
                .ToListAsync();

            return authors;
        }

        /// <summary>
        /// Create new author.
        /// </summary>
        /// <param name="newAuthor">New author data from user's input</param>
        /// <returns></returns>
        public async Task CreateAuthor(CreateAuthorViewModel newAuthor)
        {
            var author = new Author
            {
                Name = newAuthor.Name
            };

            this.DB.Author.Add(author);

            await this.DB.SaveChangesAsync();
        }

        public async Task<List<AuthorViewModel>> GetAccountAuthorsAsync(string email)
        {
            var authors = await this
                .DB
                .Author
                .AsNoTracking()
                .Where(Q => Q.Email == email)
                .Select(Q => new AuthorViewModel
                {
                    AuthorId = Q.AuthorId,
                    Name = Q.Name
                })
                .ToListAsync();

            return authors;
        }

        public async Task<PaginatedList<AuthorViewModel>> GetPage(string authorName, string orderState, int pageSize, int pageIndex)
        {
            var authorNameSort = string.IsNullOrEmpty(authorName) ? "AuthorNameDesc" : string.Empty;
            
            var query = this
                .DB
                .Author
                .AsNoTracking();

            // Check if the filter for author's name is empty or not.
            // If not, we must query the data based on the author's name.
            if (string.IsNullOrEmpty(authorName) == false)
            {
                // We can call this "query chaining".
                // The ToLower() method is used for case insensitive search, since PostgreSQL search behavior is case sensitive.
                query = query.Where(Q => Q.Name.ToLower().StartsWith(authorName.ToLower()));
            }

            switch (orderState)
            {
                case "AuthorNameDesc":
                    query = query.OrderByDescending(Q => Q.Name);
                    break;
                default:
                    query = query.OrderBy(Q => Q.Name);
                    break;
            }

            var authors = query
                .Select(Q => new AuthorViewModel
                {
                    AuthorId = Q.AuthorId,
                    Name = Q.Name
                });

            var authorsPage = await PaginatedList<AuthorViewModel>.CreateAsync(authors, pageIndex, pageSize);

            return authorsPage;
        }
    }
}
