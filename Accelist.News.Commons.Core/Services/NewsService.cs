﻿using Accelist.News.Commons.Core.Helpers;
using Accelist.News.Commons.Core.Models;
using Accelist.News.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.News.Commons.Core.Services
{
    public class NewsService
    {
        private readonly NewsDbContext DB;

        public NewsService(NewsDbContext db)
        {
            this.DB = db;
        }

        public async Task<List<NewsViewModel>> GetNewsAsync()
        {
            // Always use AsNoTracking() for performance gain.
            // Also, entity object such as News won't be tracked.
            var newsList = await this
                .DB
                .News
                .AsNoTracking()
                .OrderByDescending(Q => Q.Timestamp)
                .Select(Q => new NewsViewModel
                {
                    NewsId = Q.NewsId,
                    Title = Q.Title,
                    Content = Q.Content,
                    Timestamp = Q.Timestamp.Value
                })
                .ToListAsync();

            return newsList;
        }

        public async Task<PaginatedList<NewsViewModel>> GetPageAsync(string title, string content, string orderState, int pageSize, int pageIndex)
        {
            var authorNameSort = string.IsNullOrEmpty(title) ? "TitleDesc" : string.Empty;

            var query = this
                .DB
                .News
                .AsNoTracking();

            // Check if the filter for author's name is empty or not.
            // If not, we must query the data based on the author's name.
            if (string.IsNullOrEmpty(title) == false)
            {
                // We can call this "query chaining".
                // The ToLower() method is used for case insensitive search, since PostgreSQL search behavior is case sensitive.
                query = query.Where(Q => Q.Title.ToLower().StartsWith(title.ToLower()));
            }

            switch (orderState)
            {
                case "TitleDesc":
                    query = query.OrderByDescending(Q => Q.Title);
                    break;
                default:
                    query = query.OrderBy(Q => Q.Title);
                    break;
            }

            var newsList = query
                .Select(Q => new NewsViewModel
                {
                    Title = Q.Title,
                    Content = Q.Content
                });

            var newsPage = await PaginatedList<NewsViewModel>.CreateAsync(newsList, pageIndex, pageSize);

            return newsPage;
        }

        /// <summary>
        /// Create new news.
        /// </summary>
        /// <param name="newNews">New news data from user's input</param>
        /// <returns></returns>
        public async Task CreateNewsAsync(CreateNewsViewModel newNews)
        {
            var news = new Entities.News
            {
                Title = newNews.Title,
                Content = newNews.Content,
                AuthorId = newNews.AuthorId,
                Timestamp = DateTime.UtcNow
            };

            this.DB.News.Add(news);

            await this.DB.SaveChangesAsync();
        }
    }
}
