﻿using Accelist.News.Commons.Core.Interfaces;
using Accelist.News.Commons.Core.Models;
using Accelist.News.Commons.Core.Models.Configurations;
using Microsoft.Extensions.Options;
using Minio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.News.Commons.Core.Services
{
    /// <summary>
    /// Service class provider for providing file storage methods using MinIO services.
    /// </summary>
    public class MinioService : IFileStorageService
    {
        private readonly MinioConfiguration MinioConfiguration;

        /// <summary>
        /// Single source of truth of Minio client object in this class.
        /// </summary>
        private readonly MinioClient Minio;

        /// <summary>
        /// Default presigned expirity in seconds.
        /// </summary>
        private int PresignedExpiritySeconds => (int)TimeSpan.FromMinutes(1).TotalSeconds;

        public MinioService(IOptions<MinioConfiguration> options)
        {
            // Because we are injecting an object from the registered option in the DI container, we must use IOptions object to obtain the option values.
            this.MinioConfiguration = options.Value;

            // Setup the MinioClient object.
            this.Minio = new MinioClient(this.MinioConfiguration.EndPoint,
                this.MinioConfiguration.AccessKey,
                this.MinioConfiguration.SecretKey);
        }

        /// <summary>
        /// Insert the file to the MinIO bucket.
        /// </summary>
        /// <param name="fileStore"><seealso cref="FileStoreModel"/> object.</param>
        /// <returns></returns>
        public async Task Insert(FileStoreModel fileStore)
        {
            // Obtain the bucket name from the configuration.
            var bucketName = this.MinioConfiguration.BucketName;

            await this.VerifyBucket();

            // This will insert the file to the MinIO bucket.
            await this.Minio.PutObjectAsync(bucketName, fileStore.FileName, fileStore.Stream, fileStore.Stream.Length, fileStore.ContentType);
        }

        /// <summary>
        /// Get blob file's URL from MinIO service.
        /// </summary>
        /// <param name="fileName">The requested file name.</param>
        /// <returns>Return blob file's URL.</returns>
        public async Task<string> GetBlobUrl(string fileName)
        {
            var bucketName = this.MinioConfiguration.BucketName;

            await this.VerifyBucket();

            var url = await this.Minio.PresignedGetObjectAsync(bucketName, fileName, this.PresignedExpiritySeconds);

            return url;
        }

        /// <summary>
        /// Verify if the requested bucket is exists or not.
        /// If not exists, this method will create a new one.
        /// </summary>
        /// <returns>Return true if exists, otherwise return false.</returns>
        private async Task<bool> VerifyBucket()
        {
            var bucketName = this.MinioConfiguration.BucketName;
            var isBucketExists = await this.Minio.BucketExistsAsync(bucketName);

            // Always check if the bucket is exists or not, to ensure no exception will be thrown on this point.
            if (isBucketExists == false)
            {
                await this.Minio.MakeBucketAsync(bucketName);
            }

            return true;
        }

        /// <summary>
        /// Get blob file's data in <seealso cref="byte[]"/> format.
        /// </summary>
        /// <param name="fileName">The requested file name.</param>
        /// <returns>Return blob file's data in <seealso cref="byte[]"/> format.</returns>
        public async Task<byte[]> GetBlob(string fileName)
        {
            await this.VerifyBucket();
            var blobBytes = new byte[0];

            // WARNING: Not the best way to obtain the multiple files from MinIO.
            // This technique is just the same as load the file into web application's in-memory, which is should avoided.
            // The best way to do this is create a micro web application to load the files and compress them into a .zip file.
            // Notes that that micro web application must be hosted on another environment.
            using (var ms = new MemoryStream())
            {
                await this.Minio.GetObjectAsync(this.MinioConfiguration.BucketName, fileName, (stream) =>
                {
                    stream.CopyToAsync(ms);
                });

                blobBytes = ms.ToArray();
            }

            return blobBytes;
        }

        /// <summary>
        /// Delete blob file from minio bucket.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task Delete(string fileName)
        {
            // Obtain the bucket name from the configuration.
            var bucketName = this.MinioConfiguration.BucketName;

            await this.Minio.RemoveObjectAsync(bucketName, fileName);
        }
    }
}
