﻿using Accelist.News.Commons.Core.Models;
using Accelist.News.Entities;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Accelist.News.Commons.Core.Services
{
    /// <summary>
    /// Service class for providing various authentication and authorization functions.
    /// For cookie-based authentication: https://docs.microsoft.com/en-us/aspnet/core/security/authentication/cookie?view=aspnetcore-2.2.
    /// </summary>
    public class AuthService
    {
        public const string AuthenticationScheme = "AccelistNewsScheme";

        public const string InvalidEmailOrPasswordMessage = "Invalid email or password";

        private readonly NewsDbContext DB;

        public AuthService(NewsDbContext db)
        {
            this.DB = db;
        }

        public async Task<ClaimsPrincipal> VerifyUserAsync(LoginSubmissionModel loginInfo)
        {
            var account = await this
                .DB
                .Account
                .AsNoTracking()
                .FirstOrDefaultAsync(Q => Q.Email == loginInfo.Email);

            if (account == null)
            {
                return null;
            }

            var isValidPassword = BCrypt.Net.BCrypt.Verify(loginInfo.Password, account.Password);

            if (isValidPassword == false)
            {
                return null;
            }

            var claimsPrincipal = this.GenerateUserClaimPrincipal(loginInfo);

            return claimsPrincipal;
        }

        private ClaimsPrincipal GenerateUserClaimPrincipal(LoginSubmissionModel loginInfo)
        {
            var claimsIdentity = new ClaimsIdentity(AuthenticationScheme);
            
            // You can think claim as a cookie properties.
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Email, loginInfo.Email));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.PrimarySid, loginInfo.Email));

            var claimPrincipal = new ClaimsPrincipal(claimsIdentity);

            return claimPrincipal;
        }
    }
}
