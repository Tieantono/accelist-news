﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Accelist.News.Commons.Core.Models
{
    /// <summary>
    /// Store author data from user's inputs.
    /// </summary>
    public class CreateAuthorViewModel
    {
        /// <summary>
        /// Author name.
        /// </summary>
        [Required]
        [MinLength(8)]
        [MaxLength(255)]
        public string Name { get; set; }
    }
}
