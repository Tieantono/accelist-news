﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.News.Commons.Core.Models
{
    /// <summary>
    /// Store news information.
    /// </summary>
    public class NewsViewModel
    {
        /// <summary>
        /// News ID.
        /// </summary>
        public int NewsId { get; set; }

        /// <summary>
        /// News title.
        /// </summary>
        public string Title { get; set; }
        
        /// <summary>
        /// News content.
        /// </summary>
        public string Content { get; set; }

        public DateTime Timestamp { get; set; }
    }
}
