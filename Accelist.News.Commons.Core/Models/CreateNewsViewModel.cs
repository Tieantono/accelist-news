﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Accelist.News.Commons.Core.Models
{
    /// <summary>
    /// Model class for storing the new news data.
    /// </summary>
    public class CreateNewsViewModel
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        [Display(Name = "Use Author")]
        public int AuthorId { get; set; }
        
        public IFormFile NewsImage { get; set; }
    }
}
