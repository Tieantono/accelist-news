﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Accelist.News.Commons.Core.Models.Configurations
{
    /// <summary>
    /// MinIO configuration based on JSON structure in MinioConfig section in appsettings.
    /// </summary>
    public class MinioConfiguration
    {
        /// <summary>
        /// MinIO end point.
        /// </summary>
        public string EndPoint { set; get; }

        /// <summary>
        /// MinIO access key.
        /// </summary>
        public string AccessKey { set; get; }

        /// <summary>
        /// MinIO secret key.
        /// </summary>
        public string SecretKey { set; get; }

        /// <summary>
        /// MinIO bucket name. Usually a single app only need 1 bucket name.
        /// </summary>
        public string BucketName { set; get; }

        public string ContentType { set; get; }
    }
}
