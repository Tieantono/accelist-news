﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Accelist.News.Commons.Core.Models
{
    /// <summary>
    /// Model class for storing login inputs.
    /// </summary>
    public class LoginSubmissionModel
    {
        /// <summary>
        /// Email information.
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Password information.
        /// </summary>
        [Required]
        public string Password { get; set; }
    }
}
