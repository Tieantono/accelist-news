﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Accelist.News.Commons.Core.Models
{
    /// <summary>
    /// Store author's data.
    /// </summary>
    public class AuthorViewModel
    {
        public int AuthorId { get; set; }

        /// <summary>
        /// Store author name.
        /// </summary>
        public string Name { get; set; }
    }
}
