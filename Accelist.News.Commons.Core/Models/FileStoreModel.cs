﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Accelist.News.Commons.Core.Models
{
    /// <summary>
    /// Model class for storing file properties that will be stored via file storage service class.
    /// </summary>
    public class FileStoreModel
    {
        /// <summary>
        /// File name.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// A stream is a collection of data that coming from one source with (usually) one direction.
        /// Imagine it like a waterfall but instead of water, it contains data.
        /// Further reference: https://stackoverflow.com/questions/5144794/what-does-stream-mean-what-are-its-characteristics.
        /// </summary>
        public Stream Stream { get; set; }

        /// <summary>
        /// File content type, i.e. application/pdf for PDF file.
        /// Reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types.
        /// </summary>
        public string ContentType { get; set; }
    }
}
