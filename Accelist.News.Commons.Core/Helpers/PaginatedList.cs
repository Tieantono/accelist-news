﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.News.Commons.Core.Helpers
{
    /// <summary>
    /// A helper class for providing various pagination data.
    /// This is very useful for pagination case, and this helper class is reusable.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PaginatedList<T> : List<T>
    {
        /// <summary>
        /// Store the current page position.
        /// Using keyword private on setter means that this property can only be edited in this class.
        /// </summary>
        public int PageIndex { get; private set; }

        /// <summary>
        /// The total pages based on the queried data.
        /// </summary>
        public int TotalPages { get; private set; }

        public PaginatedList(List<T> items, int count, int pageIndex, int pageSize)
        {
            this.PageIndex = pageIndex;
            this.TotalPages = (int)Math.Ceiling(count / (double)pageSize);

            this.AddRange(items);
        }

        /// <summary>
        /// Check if the requested page data has a previous page.
        /// Can be used for displaying previous button on the UI.
        /// "=>" is a shorthand / syntatic sugar for getter.
        /// </summary>
        public bool HasPreviousPage => (PageIndex > 1);

        /// <summary>
        /// Check if the requested page data has a next page.
        /// Can be used for displaying next button on the UI.
        /// </summary>
        public bool HasNextPage => (PageIndex < TotalPages);

        /// <summary>
        /// Create the paginated list object in <seealso cref="PaginatedList{T}"/> format.
        /// The object that this method created will be used as the page data, such as all items to be displayed in the table and the total page based on the queries data.
        /// </summary>
        /// <param name="query">The query that was formed using EF Core in <seealso cref="IQueryable{T}"/> format.</param>
        /// <param name="pageIndex">The current page position.</param>
        /// <param name="pageSize">The total data to be displayed on each page.</param>
        /// <returns>Return <seealso cref="PaginatedList{T}"/> object.</returns>
        public static async Task<PaginatedList<T>> CreateAsync(IQueryable<T> query, int pageIndex, int pageSize)
        {
            // This will count all data in the related entity data in the database table.
            // i.e., this count all existing data in the author table.
            var count = await query.CountAsync();

            // Skip() will skip the data in the database based on the parameter, i.e. let's say 5, then 5 data will be skipped in the database.
            // Take() will take the amount data based on the parameter, i.e. let's say 10, then 10 data will be obtained from the database.
            /**
             * Query equivalent (in PostgreSQL):
             * SELECT *
             * FROM author
             * LIMIT 10
             * OFFSET 5
             */
            var items = await query.Skip((pageIndex - 1) * pageSize)
                .Take(pageSize).ToListAsync();

            return new PaginatedList<T>(items, count, pageIndex, pageSize);
        }
    }
}
