﻿using Accelist.News.Commons.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.News.Commons.Core.Interfaces
{
    /// <summary>
    /// Interface for defining a blueprint on how to store a file in some persistent storage, such as hard disk.
    /// </summary>
    public interface IFileStorageService
    {
        /// <summary>
        /// Insert / add blob file to the storage provider (i.e. hard disk, SQL databases, or blob container such as Minio).
        /// </summary>
        /// <param name="fileStore"><seealso cref="FileStoreModel"/> object.</param>
        /// <returns></returns>
        Task Insert(FileStoreModel fileStore);

        /// <summary>
        /// Get blob file's URL from MinIO service based on the requested file name.
        /// </summary>
        /// <param name="fileName">The requested file name.</param>
        /// <returns>Return file's URL.</returns>
        Task<string> GetBlobUrl(string fileName);

        /// <summary>
        /// Get blob file's data from MinIO service based on the requested file name.
        /// </summary>
        /// <param name="fileName">The requested file name.</param>
        /// <returns>Return file's data.</returns>
        Task<byte[]> GetBlob(string fileName);

        /// <summary>
        /// Delete blob file from storage provider.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        Task Delete(string fileName);
    }
}
