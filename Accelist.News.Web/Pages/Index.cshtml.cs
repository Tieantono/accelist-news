﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accelist.News.Commons.Core.Models;
using Accelist.News.Commons.Core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Accelist.News.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly NewsService NewsService;

        public IndexModel(NewsService newsService)
        {
            this.NewsService = newsService;
        }

        public async Task OnGetAsync()
        {
            this.NewsList = await this.NewsService.GetNewsAsync();
        }

        public List<NewsViewModel> NewsList { get; set; }
    }
}
