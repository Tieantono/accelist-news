﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Accelist.News.Commons.Core.Helpers;
using Accelist.News.Commons.Core.Models;
using Accelist.News.Commons.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Accelist.News.Web.Pages.Authors
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly AuthorService AuthorService;

        public IndexModel(AuthorService authorService)
        {
            this.AuthorService = authorService;
        }

        /// <summary>
        /// List of author data in <seealso cref="List{AuthorViewModel}"/>.
        /// </summary>
        public PaginatedList<AuthorViewModel> Authors { get; set; }

        /// <summary>
        /// Total page in number of this page.
        /// </summary>
        public int TotalPage { get; set; }

        /// <summary>
        /// Filter property for storing author's name.
        /// BindProperty annotation with SupportsGet property is set to true will enable property binding on GET method.
        /// Normally we bind a property on POST method.
        /// </summary>
        [BindProperty(SupportsGet = true)]
        [Display(Name = "Name")]
        public string AuthorName { get; set; }
        
        /// <summary>
        /// Filter property for defining which column to be ordered and order type (ascending or descending).
        /// </summary>
        [BindProperty(SupportsGet = true)]
        public string OrderState { get; set; }

        /// <summary>
        /// Filter property value.
        /// This property will tell OrderState to order by author's name.
        /// </summary>
        public string AuthorNameSort { get; set; }

        [BindProperty(SupportsGet = true)]
        public string CurrentAuthorNameFilter { get; set; }
        
        [BindProperty(SupportsGet = true)]
        public int? PageIndex { get; set; }

        public int PageSize => 5;
        
        /// <summary>
        /// Get page data.
        /// </summary>
        /// <returns></returns>
        public async Task OnGetAsync()
        {
            this.AuthorNameSort = string.IsNullOrEmpty(OrderState) ? "AuthorNameDesc" : string.Empty;

            if (this.PageIndex.HasValue == false)
            {
                this.PageIndex = 1;
            }

            if (this.AuthorName != this.CurrentAuthorNameFilter)
            {
                this.PageIndex = 1;
            }
            
            this.CurrentAuthorNameFilter = this.AuthorName;

            var pageIndex = this.PageIndex.Value;
            
            var pageData = await this.AuthorService.GetPage(this.AuthorName, this.OrderState, this.PageSize, pageIndex);

            this.TotalPage = pageData.TotalPages;
            
            // Remember that PaginatedList inherits List, so essentially they are sharing the same structure, because List is the parent of the PaginatedList.
            // So, you can assign Authors with an object that have List<AuthorViewModel> as its data type too.
            this.Authors = pageData;
        }
    }
}