﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Accelist.News.Commons.Core.Models;
using Accelist.News.Commons.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Accelist.News.Web.Pages.Authors
{
    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly AuthorService AuthorService;

        public CreateModel(AuthorService authorService)
        {
            this.AuthorService = authorService;
        }
        public void OnGet()
        {

        }
        
        [BindProperty]
        public CreateAuthorViewModel NewAuthor { get; set; }

        public async Task<ActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                TempData["Message"] = "Invalid submission data";
                return Page();
            }

            await this.AuthorService.CreateAuthor(this.NewAuthor);

            // Redirect to this page (GET).
            // Remember this pattern, GET initial page -> POST the data -> In the POST handler, return to the initial page (GET).

            TempData["Message"] = "Successfully added new author data";
            return RedirectToPage();
        }
    }
}