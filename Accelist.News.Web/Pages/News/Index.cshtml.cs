﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Accelist.News.Commons.Core.Helpers;
using Accelist.News.Commons.Core.Models;
using Accelist.News.Commons.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Accelist.News.Web.Pages.News
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly NewsService NewsService;

        public IndexModel(NewsService newsService)
        {
            this.NewsService = newsService;
        }
        /// <summary>
        /// List of news data in <seealso cref="List{NewsViewModel}"/>.
        /// </summary>
        public PaginatedList<NewsViewModel> NewsList { get; set; }

        /// <summary>
        /// Total page in number of this page.
        /// </summary>
        public int TotalPage { get; set; }

        /// <summary>
        /// Filter property for storing news' title.
        /// </summary>
        [BindProperty(SupportsGet = true)]
        public string Title { get; set; }

        [BindProperty(SupportsGet = true)]
        [Display(Name = "Content")]
        public string NewsContent { get; set; }

        /// <summary>
        /// Filter property for defining which column to be ordered and order type (ascending or descending).
        /// </summary>
        [BindProperty(SupportsGet = true)]
        public string OrderState { get; set; }

        /// <summary>
        /// This property will tell OrderState to order by news' title.
        /// </summary>
        public string TitleSort { get; set; }

        /// <summary>
        /// This property will tell OrderState to order by news' content.
        /// </summary>
        public string ContentSort { get; set; }

        [BindProperty(SupportsGet = true)]
        public string CurrentTitleFilter { get; set; }

        [BindProperty(SupportsGet = true)]
        public string CurrentContentFilter { get; set; }

        [BindProperty(SupportsGet = true)]
        public int? PageIndex { get; set; }

        public int PageSize => 5;

        public async Task OnGetAsync()
        {
            this.TitleSort = string.IsNullOrEmpty(OrderState) ? "TitleDesc" : string.Empty;

            if (this.PageIndex.HasValue == false)
            {
                this.PageIndex = 1;
            }

            if (this.Title != this.CurrentTitleFilter)
            {
                this.PageIndex = 1;
            }

            this.CurrentTitleFilter = this.Title;

            var pageIndex = this.PageIndex.Value;
            
            var pageData = await this.NewsService.GetPageAsync(this.Title, this.NewsContent, this.OrderState, this.PageSize, pageIndex);

            this.TotalPage = pageData.TotalPages;
            
            this.NewsList = pageData;
        }
    }
}