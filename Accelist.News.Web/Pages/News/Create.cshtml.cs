﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accelist.News.Commons.Core.Models;
using Accelist.News.Commons.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Accelist.News.Web.Pages.News
{
    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly NewsService NewsService;
        private readonly AuthorService AuthorService;

        public CreateModel(NewsService newsService, AuthorService authorService)
        {
            this.NewsService = newsService;
            this.AuthorService = authorService;
        }

        public async Task OnGetAsync()
        {
            // Get Email object from user's cookie / claim.
            var userEmail = this.User
                .Claims
                .Where(Q => Q.Type == ClaimTypes.Email)
                .Select(Q => Q.Value)
                .FirstOrDefault();

            // Can be optimized by invoking Select only once (in GetAccountAuthorsAsync method) by creating a new model.
            this.Authors = (await this.AuthorService.GetAccountAuthorsAsync(userEmail))
                .Select(Q => new SelectListItem
                {
                    Text = $"{ Q.Name } - ({ Q.AuthorId.ToString() })",
                    Value = Q.AuthorId.ToString()
                })
                .ToList();
        }
        
        public List<SelectListItem> Authors { get; set; }

        [BindProperty]
        public CreateNewsViewModel NewNews { get; set; }

        public async Task<ActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                TempData["Message"] = "Invalid submission data";
                return Page();
            }

            await this.NewsService.CreateNewsAsync(this.NewNews);

            // Redirect to this page (GET).
            // Remember this pattern, GET initial page -> POST the data -> In the POST handler, return to the initial page (GET).

            TempData["Message"] = "Successfully added new news data";
            return RedirectToPage();
        }
    }
}