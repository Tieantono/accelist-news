﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accelist.News.Commons.Core.Models;
using Accelist.News.Commons.Core.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Accelist.News.Web.Pages.Auth
{
    public class LoginModel : PageModel
    {
        private readonly AuthService AuthService;

        public LoginModel(AuthService authService)
        {
            this.AuthService = authService;
        }

        public void OnGet()
        {
            
        }

        [BindProperty]
        public LoginSubmissionModel LoginInfo { get; set; }


        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                TempData["ErrorMessage"] = AuthService.InvalidEmailOrPasswordMessage;

                return Page();
            }

            var claimsPrincipal = await this.AuthService.VerifyUserAsync(this.LoginInfo);

            if (claimsPrincipal == null)
            {
                TempData["ErrorMessage"] = AuthService.InvalidEmailOrPasswordMessage;

                return Page();
            }

            await HttpContext.SignInAsync(AuthService.AuthenticationScheme, claimsPrincipal);

            return RedirectToPage("/Index");
        }
    }
}