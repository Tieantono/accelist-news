﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accelist.News.Commons.Core.Models;
using Accelist.News.Commons.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace Accelist.News.Web.API
{
    [Route("api/v1/news", Name = "NewsController")]
    public class NewsController : Controller
    {
        private readonly NewsService NewsService;

        public NewsController(NewsService newsService)
        {
            this.NewsService = newsService;
        }

        [HttpGet(Name = "GetNewsAsync")]
        public async Task<ActionResult<List<NewsViewModel>>> GetAsync()
        {
            var newsList = await this.NewsService.GetNewsAsync();
            
            // Return HTTP status code 200 (OK) with news data as this HTTP response body.
            return Ok(newsList);
        }
    }
}
