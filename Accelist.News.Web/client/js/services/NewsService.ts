﻿import Axios from "axios";
import INewsModel from '../models/INewsModel';

class NewsService {
    newsList: INewsModel[] = [];
    
    async getNewsList(url: string) {
        try {
            this.newsList = (await Axios.get(url)).data;
        } catch (e) {
            alert(e);
        }
    }
}

// Create new NewsService object;
let newsSingleton = new NewsService();

// Export the object as a singleton object.
// A singleton object behave like a static object, so in the same page, any modification through this object will affect the other functions that refer this object.
export default newsSingleton;