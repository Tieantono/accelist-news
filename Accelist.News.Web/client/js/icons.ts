import { library, dom } from '@fortawesome/fontawesome-svg-core';
import {
    faChevronUp,
    faBold,
    faItalic
} from '@fortawesome/free-solid-svg-icons';
import { } from '@fortawesome/free-regular-svg-icons';

library.add(faChevronUp);
library.add(faBold);
library.add(faItalic);

// Reduce dll size by only importing icons which are actually being used:
// https://fontawesome.com/how-to-use/use-with-node-js

dom.watch();