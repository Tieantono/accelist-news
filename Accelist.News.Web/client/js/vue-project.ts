import Vue from 'vue';
import { DateTime } from 'luxon';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VeeValidate from 'vee-validate';
import Hello from './components/Hello.vue';
import { EditorMenuBar, EditorContent } from 'tiptap';
import RichTextBox from './components/RichTextBox.vue';
import NewsContainer from './components/NewsContainer.vue';
import NewsList from './components/NewsList.vue';

Vue.use(VeeValidate, {
    classes: true
});

Vue.component('fa', FontAwesomeIcon);

// components must be registered BEFORE the app root declaration
Vue.component('hello', Hello);

Vue.component('editor-menu-bar', EditorMenuBar);
Vue.component('editor-content', EditorContent);
Vue.component('rich-text-box', RichTextBox);
Vue.component('news-container', NewsContainer);
Vue.component('news-list', NewsList);

// You can register your custom common text formatting with Vue.js Filters feature.
// By using common text formatting, you can just apply the text format to all existing Vue templates by including the filter name to your template.
Vue.filter('formatDate', (value) => {
    if (value !== null || value !== undefined) {
        return DateTime
            .fromISO(value)
            .toFormat('yyyy-MM-dd HH:mm');
    }

    return '';
})

// bootstrap the Vue app from the root element <div id="app"></div>
new Vue().$mount('#app');
