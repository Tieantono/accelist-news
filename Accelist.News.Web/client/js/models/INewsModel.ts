﻿/**
 * Interface for defining data type of news item.
 * */
export default interface INewsModel {
    newsId: number;
    title: string;
    content: string;
    timestamp: string;
}