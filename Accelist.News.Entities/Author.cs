﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.News.Entities
{
    [Table("author")]
    public partial class Author
    {
        public Author()
        {
            News = new HashSet<News>();
        }

        [Column("author_id")]
        public int AuthorId { get; set; }
        [Required]
        [Column("name")]
        public string Name { get; set; }
        [Required]
        [Column("email")]
        public string Email { get; set; }

        [ForeignKey("Email")]
        [InverseProperty("Author")]
        public virtual Account EmailNavigation { get; set; }
        [InverseProperty("Author")]
        public virtual ICollection<News> News { get; set; }
    }
}
