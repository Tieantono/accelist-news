﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.News.Entities
{
    [Table("account")]
    public partial class Account
    {
        public Account()
        {
            Author = new HashSet<Author>();
        }

        [Column("email")]
        public string Email { get; set; }
        [Required]
        [Column("password")]
        public string Password { get; set; }
        [Column("timestamp", TypeName = "timestamp with time zone")]
        public DateTime Timestamp { get; set; }

        [InverseProperty("EmailNavigation")]
        public virtual ICollection<Author> Author { get; set; }
    }
}
