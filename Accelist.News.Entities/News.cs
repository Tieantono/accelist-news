﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.News.Entities
{
    [Table("news")]
    public partial class News
    {
        [Column("news_id")]
        public int NewsId { get; set; }
        [Required]
        [Column("title")]
        public string Title { get; set; }
        [Required]
        [Column("content")]
        public string Content { get; set; }
        [Column("news_blob_id")]
        public Guid? NewsBlobId { get; set; }
        [Column("author_id")]
        public int AuthorId { get; set; }
        [Column("timestamp", TypeName = "timestamp with time zone")]
        public DateTime? Timestamp { get; set; }

        [ForeignKey("AuthorId")]
        [InverseProperty("News")]
        public virtual Author Author { get; set; }
        [ForeignKey("NewsBlobId")]
        [InverseProperty("News")]
        public virtual Blob NewsBlob { get; set; }
    }
}
