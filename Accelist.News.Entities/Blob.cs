﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.News.Entities
{
    [Table("blob")]
    public partial class Blob
    {
        public Blob()
        {
            News = new HashSet<News>();
        }

        [Column("blob_id")]
        public Guid BlobId { get; set; }
        [Required]
        [Column("extension")]
        public string Extension { get; set; }
        [Required]
        [Column("content_type")]
        public string ContentType { get; set; }
        [Column("created_at", TypeName = "timestamp with time zone")]
        public DateTime CreatedAt { get; set; }

        [InverseProperty("NewsBlob")]
        public virtual ICollection<News> News { get; set; }
    }
}
