﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Accelist.News.Entities
{
    public partial class NewsDbContext : DbContext
    {
        public NewsDbContext(DbContextOptions<NewsDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Author> Author { get; set; }
        public virtual DbSet<Blob> Blob { get; set; }
        public virtual DbSet<News> News { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("uuid-ossp")
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasKey(e => e.Email)
                    .HasName("pk_user");

                entity.Property(e => e.Email).ValueGeneratedNever();

                entity.Property(e => e.Timestamp).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<Author>(entity =>
            {
                entity.Property(e => e.AuthorId).UseNpgsqlIdentityAlwaysColumn();

                entity.HasOne(d => d.EmailNavigation)
                    .WithMany(p => p.Author)
                    .HasForeignKey(d => d.Email)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_author__account");
            });

            modelBuilder.Entity<Blob>(entity =>
            {
                entity.Property(e => e.BlobId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("CURRENT_TIMESTAMP");
            });

            modelBuilder.Entity<News>(entity =>
            {
                entity.Property(e => e.NewsId).UseNpgsqlIdentityAlwaysColumn();

                entity.HasOne(d => d.Author)
                    .WithMany(p => p.News)
                    .HasForeignKey(d => d.AuthorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_news__author");

                entity.HasOne(d => d.NewsBlob)
                    .WithMany(p => p.News)
                    .HasForeignKey(d => d.NewsBlobId)
                    .HasConstraintName("fk_news__blob");
            });
        }
    }
}
