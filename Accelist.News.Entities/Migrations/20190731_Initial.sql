﻿-- PascalCase = MyObject (C#)
-- snake_case = my_object (PostgreSQL)
-- camelCase = myObject (JavaScript, TypeScript, C#)

-- Untuk menyimpan data-data sang penulis.
CREATE TABLE author(
	author_id INT GENERATED ALWAYS AS IDENTITY -- For auto-generate the ID number.
		CONSTRAINT pk_author PRIMARY KEY,
	name TEXT NOT NULL, -- Author's name
	
	CONSTRAINT check_name CHECK (char_length(name) <= 255) -- Constraint for check allowed 'name' character length.
);

INSERT INTO author(name)
VALUES('Budi');

-- For storing blob data references such as file name, file extension, content-type.
CREATE TABLE blob(
	blob_id UUID
		CONSTRAINT pk_blob PRIMARY KEY,
	
	extension TEXT NOT NULL, -- Store file extension, i.e. .jpg, .png, .zip, etc.
	
	-- Store content-type, i.e. image/jpg, image/png, application/zip, etc.
	-- For further references, please look up on the search engine such as Google with keyword: "content-type".
	content_type TEXT NOT NULL,
	
	created_at TIMESTAMPTZ NOT NULL
		DEFAULT CURRENT_TIMESTAMP
);

-- Store news/articles data.
CREATE TABLE news(
	news_id INT GENERATED ALWAYS AS IDENTITY
		CONSTRAINT pk_news PRIMARY KEY,
	
	title TEXT NOT NULL,
	content TEXT NOT NULL,
	
	news_blob_id UUID
		CONSTRAINT fk_news__blob REFERENCES blob NULL,
	
	author_id INT
		CONSTRAINT fk_news__author REFERENCES author NOT NULL,
	
	timestamp TIMESTAMPTZ -- Data type for storing datetime value (plus timezone).
);