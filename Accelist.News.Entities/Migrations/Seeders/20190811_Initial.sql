﻿-- Using BCrypt hashing algorithm (12 rounds), the current password value is equivalent as "HelloWorld".
INSERT INTO account
VALUES('admin@news.accelist.com', '$2y$12$3QAVUsSVeCBnA6kxdqyQx.jkt7BcePB6HnWJtGvCL9DGnjBwUS.Om');

INSERT INTO author(name, email)
VALUES('News Admin', 'admin@news.accelist.com');